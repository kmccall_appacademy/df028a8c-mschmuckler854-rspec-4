class Book
    attr_accessor :title
    def title=(title)
        insignif = ["the", "a", "and", "an", "in", "of"]
        sentence = title.split(" ")
        final = [sentence[0].capitalize]
        sentence[1..-1].map! do |item|
            if insignif.include?(item)
                final << item.downcase
                else
                final << item.capitalize
            end
        end
        @title = final.join(" ")
    end
end
