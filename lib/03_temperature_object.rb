class Temperature
    attr_accessor :heat
    attr_accessor :type
    def initialize(input)
        @heat = input.values[0]
        @type = input.keys[0]
    end
    
    def in_fahrenheit
        if @type == :c
            @heat * (9.0/5.0) + 32
        else
            @heat
        end
    end
    
    def in_celsius
        if @type == :f
            (@heat - 32) * (5.0/9.0)
        else
            @heat
        end
    end
    
    def self.from_celsius(heat)
    self.new({ :c => heat })
    end

    def self.from_fahrenheit(heat)
    self.new({ :f => heat })
    end
end

class Fahrenheit < Temperature
    attr_accessor :heat
    attr_accessor :type
    def initialize(heat)
        @heat = heat
        @type = :f
    end
end

class Celsius < Temperature
    attr_accessor :heat
    attr_accessor :type
    def initialize(heat)
        @heat = heat
        @type = :c
    end
end
