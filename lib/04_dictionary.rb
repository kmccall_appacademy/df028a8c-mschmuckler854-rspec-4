class Dictionary
    attr_reader :entries
    def initialize
        @entries = Hash.new(nil)
    end
    
    def add(input)
        if input.is_a?(Hash)
            @entries[(input.keys)[0]] = (input.values)[0]
            else
            @entries[input] = nil
        end
    end
    
    def keywords
        @entries.keys.sort
    end
    
    def include?(input)
        @entries.include?(input)
    end
    
    def find(input)
        final = {}
        @entries.keys.each do |key|
            final[key] = @entries[key] if key.include?(input)
        end
        final
    end
    
    def printable
        final = []
        @entries.each do |k, v|
            final << "[#{k}] \"#{v}\""
        end
        final.sort.join("\n")
    end
    
end
