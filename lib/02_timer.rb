class Timer
    attr_writer :seconds
    def seconds(time=0)
        @seconds = time
    end
    
    def padded(string)
        if string.length == 1
            "0#{string}"
        else
            string
        end
    end
    
    def time_string
        seconds_ = @seconds
        minutes_ = seconds_ / 60
        hours_ = minutes_ / 60
        
        seconds_ -= minutes_ * 60
        minutes_ -= hours_ * 60
        
        "#{padded(hours_.to_s)}:#{padded(minutes_.to_s)}:#{padded(seconds_.to_s)}"
    end
end
